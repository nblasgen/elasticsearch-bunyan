var gulp = require('gulp'),
	merge2 = require('merge2'),
	apidoc = require('gulp-apidoc'),
	babel = require('gulp-babel'),
	eslint = require('gulp-eslint'),
	mocha = require('gulp-mocha'),
	nodemon = require('gulp-nodemon'),
	del = require('del');

// Process params
var runArgs = process.argv.slice(3),
	args = {},
	i;

for(i = 0; i < runArgs.length; i+=2) {
	if(runArgs[i] && runArgs[i + 1]) {
		args[runArgs[i].replace('--', '')] = runArgs[i + 1];
	}
}

// Tasks
gulp.task('clean', function () {
	return del(['dist', '.gulp-cache']);
});

gulp.task('lint', function() {
	var stream = gulp.src('src/**/*.js')
		.pipe(eslint())
		.pipe(eslint.format())
		.pipe(eslint.failOnError());
	return stream;
});

function pugSteamFn() {
	var stream = gulp.src('./src/**/*.pug'); // write them
	return stream;	
}
gulp.task('pug', pugSteamFn);

function buildSteamFn() {
	var stream = gulp.src('./src/**/*.js') // your ES2015 code
		//.pipe(cache.filter()) // remember files
		.pipe(babel()); // compile new ones
	return stream; // important for gulp-nodemon to wait for completion
}
gulp.task('build', buildSteamFn);

gulp.task('compile', ['clean', 'lint'], function () {	
	return merge2(
		buildSteamFn(),
		pugSteamFn()
	)
	.pipe(gulp.dest('./dist'))
});

gulp.task('test', ['compile'], function () {
	var stream = gulp.src(['tests/init.js'])
		.pipe(mocha({
			reporter: 'spec' /* nyan / spec / doc */
		}))
		.once('end', function () {
			process.exit();
		});

	return stream;
});

gulp.task('server', ['compile'], function (done) {
	nodemon({
		script: 'dist/app', // run ES5 code
		watch: ['src', 'config'], // watch ES2015 code
		env: { 
			'NODE_ENV': args.env || 'development' 
		},
		tasks: ['compile'], // compile synchronously onChange
		stdout: false
	})
	.on('readable', function () {
		// Pass output through bunyan formatter
		var bunyan = childProcess.fork(
			path.join('.', 'node_modules', '.bin', 'bunyan'),
			[],
			{silent: true}
		);

		bunyan.stdout.pipe(process.stdout);
		bunyan.stderr.pipe(process.stderr);
		this.stdout.pipe(bunyan.stdin);
		this.stderr.pipe(bunyan.stdin);
	});
	done();
});

gulp.task('start', ['compile'], function () {
	// gulp start --env localhost
	// compile and watch
	var stream = nodemon({
		script: 'dist/app', // run ES5 code
		watch: ['src', 'config'], // watch ES2015 code
		env: { 
			'NODE_ENV': args.env || 'development' 
		},
		tasks: ['compile'] // compile synchronously onChange
	});

	return stream;
});

gulp.task('apidoc', function (done) {
	apidoc({
		src: "src/",
		dest: "doc/"
	}, done);
});
